use crate::rps::Weapon;
use core::cmp::Ordering;

/// Result of `RpsGame`
#[repr(C)]
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum GameResult {
    /// Player has won the game
    Win {
        /// The weapon the player chose
        player: Weapon,
        /// The weapon the robot chose
        robo: Weapon,
    },
    /// Player has lost the game
    Lose {
        /// The weapon the player chose
        player: Weapon,
        /// The weapon the robot chose
        robo: Weapon,
    },
    /// It is a draw
    Draw {
        /// The weapon the player chose
        player: Weapon,
        /// The weapon the robot chose
        robo: Weapon,
    },
}

/// Rock Paper scissors Game
#[repr(C)]
#[derive(
    Copy, Clone, Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash,
)]
pub struct RpsGame {
    // Player's chosen weapon
    pub(crate) player: Weapon,
    // Bot's chosen Weapon
    pub(crate) robo: Weapon,
}

impl RpsGame {
    /// Create a new game of rock paper scissors
    /// weapon: Weapon you chose to use (Rock/Paper/Scissors)

    pub fn new_game(weapon: Weapon) -> Self {
        Self {
            player: weapon,

            robo: Weapon::rand(),
        }
    }
    /// convert the struct into a result of the game
    pub fn result(self) -> GameResult {
        self.into()
    }
}

impl Into<GameResult> for RpsGame {
    fn into(self) -> GameResult {
        match self.player.cmp(&self.robo) {
            Ordering::Greater => GameResult::Win {
                player: self.player,
                robo: self.robo,
            },
            Ordering::Equal => GameResult::Draw {
                player: self.player,
                robo: self.robo,
            },
            Ordering::Less => GameResult::Lose {
                player: self.player,
                robo: self.robo,
            },
        }
    }
}
