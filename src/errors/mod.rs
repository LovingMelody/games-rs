//! Errors for Games-rs

use core::fmt;

/// Base Error code for Card errors (1XXX)
pub const BASE_CARD_ERROR_CODE: i32 = 1000;
/// Base Error code for BlackJack erros (2XXX)
pub const BASE_BLACKJACK_ERROR_CODE: i32 = 2000;
/// Base Error code for Solitaire (3XXX)
pub const BASE_SOLITAIRE_ERROR_CODE: i32 = 3000;
/// Base Error code for Rock Paper Scissors (4XXX)
pub const BASE_ROCK_PAPER_SCISSORS_ERROR_CODE: i32 = 4000;
/// Base Error code for Coin Toss (5XXX)
pub const BASE_COIN_TOSS_ERROR_CODE: i32 = 5000;
/// Base Error code for Deck errors (6XXX)
pub const BASE_DECK_ERROR_CODE: i32 = 6000;
/// Base Error code for SlotMachine (7XXX)
pub const BASE_SLOT_MACHINE_ERROR_CODE: i32 = 7000;

/// A Generic Error for simplification
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct GenericError {
    /// Error Code
    pub error_code: i32,
    /// Error message
    pub error_message: String,
}

impl fmt::Display for GenericError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error ({}): {}", self.error_code, self.error_message)
    }
}

/// Error code trait
/// Note: This trait is Temporary, waiting for std::proces::Termination to be stabalized. (#43301)
pub trait ErrorCode: fmt::Debug {
    /// Return error code
    fn error_code(&self) -> i32;
}

impl<E: ErrorCode> From<E> for GenericError {
    fn from(err: E) -> Self {
        Self {
            error_code: err.error_code(),
            error_message: format!("{:?}", err),
        }
    }
}
