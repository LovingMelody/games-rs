//! A 3 Column slot machine with 7 possible characters per column
//! ```
//! let slots = games::slot_machine::SlotMachine::new();
//! let mut picks = slots.picks();
//! if picks[0] == picks[1] || picks[0] == picks[2] || picks[1] == picks[2] {
//!     if picks[0] == picks[1] && picks[1] == picks[2] {
//!         println!("You have 3 matching characters!")
//!     } else {
//!         println!("You have 2 matching characters!")
//!     }
//! } else {
//!     println!("You have no matching characters")
//! }

use rand::Rng;

const ROW_LEN: usize = 7;
const ROW: [char; ROW_LEN] = ['🍒', '🍊', '🍓', '🍍', '🍇', '🍉', '⭐'];

/// Slot machine
#[repr(C)]
#[derive(
    Clone, Copy, Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash,
)]
pub struct SlotMachine([char; 3]);

impl SlotMachine {
    /// Spin the slot machine
    pub fn new() -> Self {
        Self::default()
    }

    /// The 3 choices chosen at random by slot machine
    pub fn picks(self) -> [char; 3] {
        self.0
    }
}

impl Default for SlotMachine {
    fn default() -> Self {
        let mut rng = crate::get_rng();
        let mut picks = ['\0', '\0', '\0'];
        picks[0] = ROW[rng.gen_range(0..ROW_LEN)];
        picks[1] = ROW[rng.gen_range(0..ROW_LEN)];
        picks[2] = ROW[rng.gen_range(0..ROW_LEN)];

        SlotMachine(picks)
    }
}
