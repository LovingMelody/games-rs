use crate::errors::BASE_BLACKJACK_ERROR_CODE;
use core::fmt;

/// Blackjack errors
#[repr(C)]
#[derive(Debug, Copy, Clone, serde::Serialize, serde::Deserialize, Eq, PartialEq, Hash)]
pub enum BlackJackError {
    /// Player has lost the game, no other actions can be made. (2001)
    PlayerLostError,
    /// Player has won the game, no other actions can be made. (2002)
    PlayerWonError,
    /// Game Ran out of cards (2003)
    OutOfCardsError,
}

impl fmt::Display for BlackJackError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use BlackJackError::*;
        match *self {
            PlayerLostError => {
                f.write_str("Player has lost the game, no other actions can be made.")
            }
            PlayerWonError => f.write_str("Player has won the game, no other actions can be made."),
            OutOfCardsError => f.write_str("The game has run out of cards in the deck."),
        }
    }
}

impl crate::errors::ErrorCode for BlackJackError {
    fn error_code(&self) -> i32 {
        BASE_BLACKJACK_ERROR_CODE
            + match *self {
                BlackJackError::PlayerLostError => 1,
                BlackJackError::PlayerWonError => 2,
                BlackJackError::OutOfCardsError => 3,
            }
    }
}
