//! Your typical BlackJack game
//! ```
//! use games::blackjack::{BlackJack, GameState};
//! let mut bj = BlackJack::default(); // Only use 1 deck, use `BlackJack::new_game(5)` for 5 decks
//! bj.player_hit().is_ok(); // Make a move
//!                          // Game can be frozen with `bj.freeze()`;
//!                          // And can be defrosted with `BlackJack::defrost`
//! match bj.finish() {
//!     GameState::PlayerWon => println!("You win!"),
//!     GameState::PlayerLost => println!("You lose!"),
//!     _ => unreachable!(), // Only PlayerWon / PlayerLost is returned
//! }
//! ```

/// Errors for blackjack
mod errors;
mod game;
mod hand;
pub use self::errors::BlackJackError;
pub use self::game::{BlackJack, GameDisplay, GameState};
