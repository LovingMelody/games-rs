/// Alias for solitaire
pub type SError<T> = Result<T, SolitaireError>;

use crate::errors::{ErrorCode, BASE_SOLITAIRE_ERROR_CODE};
use core::fmt::{self, Display};

/// Solitaire Errors
#[repr(C)]
#[derive(Copy, Clone, Debug, serde::Serialize, serde::Deserialize, Hash, PartialEq, Eq)]
pub enum SolitaireError {
    /// Invalid movement (3001)
    IllegalMovementError,
    /// Out of range movement (3002)
    OutOfRangeError,
    /// Out of cards, should be impossible (3003)
    OutOfCardsError,
    /// Out of moves, exceeded u32 limit (3004)
    OutOfMoves,
}

impl Display for SolitaireError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use SolitaireError::*;
        match *self {
            IllegalMovementError => f.write_str("Illegal Movement"),
            OutOfRangeError => {
                f.write_str("Requested movement extends beyond any position cards lay")
            }
            OutOfCardsError => f.write_str("Out of cards"),
            OutOfMoves => f.write_str("Out of moves"),
        }
    }
}

impl ErrorCode for SolitaireError {
    fn error_code(&self) -> i32 {
        BASE_SOLITAIRE_ERROR_CODE
            + match *self {
                SolitaireError::IllegalMovementError => 1,
                SolitaireError::OutOfRangeError => 2,
                SolitaireError::OutOfCardsError => 3,
                SolitaireError::OutOfMoves => 4,
            }
    }
}
