# Change Log
Until version 1.0 0.x.y changes may include breaking changes however, y changes will not.


# 0.2.1
---
Bugfix: Fix logic error in `BlackJack`'s dealer logic that doesn't stop the dealer from drawing at 17 points


## 0.2.0
---
Update: Dependencies are versioned up to up to minor, so that patches can be made without crate updates.

Update: Documentation for all parts of the crate has been updated

Breaking: `deck_of_cards` is now removed, use `Deck::default()` instead

New: Added feature `small_rng` to support rand's small_rng feature which performs better but is more insecure

### Changes for `deck`

Update: Decks are now generic, and support defaults for all cards that implment `DefaultCollection`

Add: DefaultCollection trait, so that cards can have default decks

Breaking: `Deck` can no longer be configured to draw randomly. Use `Deck::shuffle` or `Deck::draw_random` method

Add: Added ease of life methods
- `Deck::draw_rest`
- `Deck::is_empty`
- `Deck::peek`
- `Deck::from_cards`
- `Dec::len`
- `Deck::empty`

Add: Added  the following methods for random opertations:
- `Deck::draw_random`
- `Deck::draw_random_many`
- `Deck::peek`
- `Deck::draw_rest_random`

Update: `Deck` now implements `Extend`, `FromIter`, and `IntoIter

Update: `Deck now uses VecDeque internally

Add: `DeckBuilder` to build decks with more sets

### Changes for `slot_machine`:

Add: `SlotMachine::picks` to get results from spin

Breaking: field `picks` for `SlotMachine` no longer exists use method `SlotMachine::picks` instead

### Changes for `rps`:

Breaking: `RpsGame::new` no longer takes `&str`, now takes a `Weapon` and only returns `RpsGame`

### Changes for `coin_toss::*`:

Breaking: `CoinToss::guess` now takes a `Coin` and returns `CoinToss`

Breaking: Removed `CoinToss::guess_coin`, use `CoinToss:guess`

Add: Implemented `FromStr` for `Coin`

### Changes for `StandardCard`:

Breaking: Renamed `StandardCard::face_as_string` to `StandardCard::face_to_string` to better express what the method does

### Changes for `black_jack::*`

Breaking: Moved `black_jack::errors::*` to `black_jack`

Breaking: `BlackJack::new_game` now takes a `usize`, instead of any type that implements `Into<Option<usize>>`

Breaking: Rename `BlackJack::defreeze` to `BlackJack::dethaw`

Add: `BlackJack::display_game` to display game

Add: `black_jack::GameDisplay` to display games


Add: Added a new game, `Solitaire`
