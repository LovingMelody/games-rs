use games::blackjack::*;

fn main() {
    let mut bj = BlackJack::default(); // Only use 1 deck, use `BlackJack::new_game(5)` for 5 decks
    println!("{}", bj.display_game());
    let _ = bj.player_hit().is_ok(); // Make a move
                                     // Game can be frozen with `bj.freeze()`;
                                     // And can be defrosted with `BlackJack::defrost`

    println!("{}", bj.display_game());
    match bj.finish() {
        GameState::PlayerWon => println!("You win!"),
        GameState::PlayerLost => println!("You lose!"),
        _ => unreachable!(), // Only PlayerWon / PlayerLost is returned
    }
}
