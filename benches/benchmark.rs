#![allow(non_snake_case)]
#[macro_use]
extern crate criterion;

extern crate games;

criterion_group!(
    BlackJack,
    black_jack::new_game,
    black_jack::freeze,
    black_jack::defrost,
    black_jack::hit,
    black_jack::finish,
);
criterion_group!(CoinToss, coin_toss::new_game);
criterion_group!(RockPaperScissors, rps_game::new_game);
criterion_group!(SlotMachine, slot_machine::slots);

criterion_group!(Solitaire, solitaire::new_game);
criterion_group!(Deck, deck_creation::deck);
criterion_main!(
    BlackJack,
    CoinToss,
    RockPaperScissors,
    SlotMachine,
    Solitaire,
    Deck
);

mod black_jack {
    use criterion::Criterion;
    use games::blackjack::*;
    pub fn new_game(c: &mut Criterion) {
        c.bench_function("New Game (Default)", |b| b.iter(BlackJack::default));
        c.bench_function_over_inputs(
            "New Game (Variable size)",
            |b, s| b.iter(|| BlackJack::new_game(**s)),
            [2usize, 4usize, 6usize, 8usize].iter(),
        );
    }
    pub fn freeze(c: &mut Criterion) {
        c.bench_function("Freeze New Game clone", |b| {
            let bj = BlackJack::default();
            b.iter(|| bj.clone().freeze())
        });
    }
    pub fn defrost(c: &mut Criterion) {
        c.bench_function("Defrost New Game", |b| {
            if let Ok(fbj) = BlackJack::default().freeze() {
                b.iter(move || BlackJack::defrost(fbj.clone()))
            }
        });
    }
    pub fn hit(c: &mut Criterion) {
        c.bench_function("Hit", move |b| {
            let bj = BlackJack::default();

            b.iter(move || {
                let mut bj = bj.clone();
                let _ = bj.player_hit().is_ok();
            })
        });
    }
    pub fn finish(c: &mut Criterion) {
        c.bench_function("Finish", |b| {
            let bj = BlackJack::default();
            b.iter(move || {
                let _result = bj.clone().finish();
            })
        });
    }
    #[allow(dead_code)]
    pub fn hit_n_finish(c: &mut Criterion) {
        c.bench_function("Hit & Finish", |b| {
            let bj = BlackJack::default();
            b.iter(move || {
                let mut bj = bj.clone();
                let _ = bj.player_hit().is_ok();
                bj.finish();
            })
        });
    }
}
mod coin_toss {
    use criterion::Criterion;
    use games::coin_toss::*;
    pub fn new_game(c: &mut Criterion) {
        c.bench_function_over_inputs(
            "Coin Toss",
            |b, &guess| b.iter(|| CoinToss::guess(*guess)),
            &[Coin::Heads, Coin::Tails],
        );
    }
}
mod rps_game {
    use criterion::Criterion;
    use games::rps::*;
    pub fn new_game(c: &mut Criterion) {
        c.bench_function_over_inputs(
            "Rock Paper Scissors",
            |b, &wep| b.iter(|| RpsGame::new_game(*wep).result()),
            &[Weapon::Rock, Weapon::Paper, Weapon::Scissors],
        );
    }
}
mod slot_machine {
    use criterion::Criterion;
    use games::slot_machine::SlotMachine;
    pub fn slots(c: &mut Criterion) {
        c.bench_function("Slot Machine", |b| b.iter(SlotMachine::new));
    }
}

mod solitaire {
    use criterion::Criterion;
    use games::solitaire::Solitaire;
    pub fn new_game(c: &mut Criterion) {
        c.bench_function("New Solitaire Game", |b| b.iter(Solitaire::new));
    }
}

mod deck_creation {
    use criterion::Criterion;
    use games::cards::StandardCard;
    use games::deck::Deck;
    pub fn deck(c: &mut Criterion) {
        c.bench_function("Create new deck", |b| {
            b.iter(|| {
                let _: Deck<StandardCard> = Deck::default();
            })
        });
    }
}
