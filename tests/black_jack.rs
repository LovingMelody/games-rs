use games::blackjack;

#[test]
fn core() {
    for n in 0..10usize {
        blackjack::BlackJack::new_game(n)
            .freeze()
            .map(blackjack::BlackJack::defrost)
            .expect("defrosted blackjack game");
    }
}
