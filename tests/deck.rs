use ::games::cards::StandardCard;
use ::games::deck;

// Verify random draw wont effect anything, other than performance
#[test]
fn draw_many() {
    // Over Draw
    {
        let mut d1: deck::Deck<StandardCard> = deck::Deck::default();
        let mut d2: deck::Deck<StandardCard> = deck::Deck::default();

        assert_eq!(52usize, d1.draw_many_random(128).len());
        // Over Draw
        assert_eq!(52usize, d2.draw_many(128).len());
        assert!(d2.is_empty());
        assert!(d1.is_empty());
    }
    // Draw only a few
    {
        let mut d1: deck::Deck<StandardCard> = deck::Deck::default();
        assert_eq!(28usize, d1.draw_many_random(28).len());
        assert_eq!(24usize, d1.len());
    }
    {
        let mut d2: deck::Deck<StandardCard> = deck::Deck::default();
        assert_eq!(28usize, d2.draw_many(28).len());
        assert_eq!(24usize, d2.len());
    }
}

#[test]
fn draw() {
    let mut d1: deck::Deck<StandardCard> = deck::Deck::default();
    let mut d2: deck::Deck<StandardCard> = deck::DeckBuilder::default().build();
    // Empty the deck by drawing cards (No errors should be raised)
    for _ in 0..d1.len() {
        d1.draw().expect("Expected to get a card");
    }
    for _ in 0..d2.len() {
        d2.draw_random().expect("Expected to get a card");
    }
    // Verify they are empty
    assert!(d1.is_empty());
    assert!(d2.is_empty());
    // Verify draw doesn't panic, but throws an error
    d1.draw().expect_err("Expected an error");
    d2.draw_random().expect_err("Expected an error");
}
#[test]
fn create() {
    let deck: deck::Deck<StandardCard> = deck::DeckBuilder::default().sets(0).build();
    assert_eq!(deck.len(), 0); // 0 sets of 52
    let deck: deck::Deck<StandardCard> = deck::Deck::default();
    assert_eq!(deck.len(), 52); // 1 sets of 52
    let deck: deck::Deck<StandardCard> = deck::DeckBuilder::default().sets(3).build();
    assert_eq!(deck.len(), 156); // 3 sets of 52
}
